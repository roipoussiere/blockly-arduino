# Arduino for Blockly

## Installation

    yarn add --save blockly-arduino

## Usage

Blockly Arduino adds its generator in the Blockly.Arduino namespace, and its blocks in Blockly.Blocks.

### Import blockly-arduino via ES Module

Import Blockly:

```js
import * as Blockly from 'blockly/core'
import 'blockly/blocks'
```

Import both Arduino generator and blocks:

```js
import 'blockly-arduino'
```

Or separately:

```js
import 'blockly-arduino/arduino'
import 'blockly-arduino/blocks'
```

## Contributing

Build package:

    yarn build
