'use strict';

import * as Blockly from 'blockly/core'
import './dist/arduino'
import './dist/blocks'

const workspace = Blockly.inject (
	'blockly', {
		'grid': {
			'colour': '#ccc',
			'length': 3,
			'snap': true,
			'spacing': 25
		},
		'media': './media/',
		'scrollbars': true,
		'toolbox': document.getElementById ('toolbox'),
		'zoom': { 'controls': true }
	}
);

workspace.addChangeListener (function() {
	var code = Blockly.Arduino.workspaceToCode(workspace);
	document.getElementById('textarea').value = code;
});
