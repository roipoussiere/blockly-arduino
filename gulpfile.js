'use strict';

/* eslint-env node */

const { series, parallel, src, dest } = require ('gulp');
const concat = require ('gulp-concat');
const insert = require ('gulp-insert');
const del = require ('del');
const uglify = require ('gulp-uglify');
const umd = require ('gulp-umd');

/** Wraps blocks into a UMD module (ie. `import 'blockly-arduino/blocks';`) */
function packageBlocks () {
	return src ('src/blocks/*.js').
		pipe (concat ('blocks.js')).
		pipe (insert.prepend (`var goog = goog || {
	require:function(){},provide:function(){},
	isFunction:function(f){return f && {}.toString.call(f)==='[object Function]'}};`)).
		pipe (umd ({
			'dependencies' () {
				return [ {
					'amd': 'blockly',
					'cjs': 'blockly',
					'global': 'Blockly',
					'name': 'Blockly',
					'param': 'Blockly'
				} ];
			},
			'exports' () {
				return null;
			},
			'namespace' () {
				return 'Blockly.Blocks.Arduino';
			}
		})).
		pipe (uglify ()).
		pipe (dest ('dist'));
}

/** Wraps generator into a UMD module (ie. `import 'blockly-arduino/arduino';`) */
function packageArduino () {
	return src ([
		'src/generators/arduino.js', 'src/generators/arduino/*.js'
	]).
		pipe (concat ('arduino.js')).
		pipe (insert.prepend (`var goog = goog || {
	require:function(){},provide:function(){},
	isFunction:function(f){return f && {}.toString.call(f)==='[object Function]'}};`)).
		pipe (umd ({
			'dependencies' () {
				return [ {
					'amd': 'blockly',
					'cjs': 'blockly',
					'global': 'Blockly',
					'name': 'Blockly.arduino',
					'param': 'Blockly'
				} ];
			},
			'exports' () {
				return 'Blockly.Arduino';
			},
			'namespace' () {
				return 'Blockly.Arduino';
			}
		})).
		pipe (uglify ()).
		pipe (dest ('dist'));
}

/** Wraps blocks and generator into a UMD module. (ie. `import 'blockly-arduino';`) */
function packageIndex () {
	return src ([
		'src/generators/arduino.js', 'src/generators/arduino/*.js', 'src/blocks/*.js'
	]).
		pipe (concat ('main.js')).
		pipe (insert.prepend (`var goog = goog || {
	require:function(){},provide:function(){},
	isFunction:function(f){return f && {}.toString.call(f)==='[object Function]'}};`)).
		pipe (umd ({
			'dependencies' () {
				return [ {
					'amd': 'blockly',
					'cjs': 'blockly',
					'global': 'Blockly',
					'name': 'Blockly.Arduino',
					'param': 'Blockly'
				} ];
			},
			'exports' () {
				return 'Blockly.Arduino';
			},
			'namespace' () {
				return 'Blockly.Arduino';
			}
		})).
		pipe (uglify ()).
		pipe (dest ('dist'));
}

/** Copies the package.json file into the dist directory. */
function packageJson () {
	return src ('./package.json').pipe (dest ('dist'));
}

/** Copies the README.md file into the distribution dist (will be displayed in npm page). */
function packageReadme () {
	return src ('./README.md').pipe (dest ('dist'));
}

/** Copies the LICENSE file into the dist directory. */
function packageLicense () {
	return src ('./LICENSE').pipe (dest ('dist'));
}

/** Clean dist directory */
function clean () {
	return del ([ 'dist' ]);
}

const pack = parallel (
	packageBlocks,
	packageArduino,
	packageIndex,
	packageJson,
	packageReadme,
	packageLicense
);

/** Prepares Blockly Arduino for an npm release, generating files under the dist directory. */
exports.clean = clean;
exports.build = series (clean, pack);
